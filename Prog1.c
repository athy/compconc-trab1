/*
 ============================================================================
 Name        : Prog1.c
 Author      : Arthur Bit-Monnot
               Marcelo Araújo Carvalho
               Jonatas Handam Santos
 Description : Matrix manipulation with a single process
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>

#include "MatrixTools.h"



int main(int argc, char *argv[])
{
    /** Declarations */
    int m, k, i;
    float *matrix;
    float *products;
    struct timespec startUpTime, endTime;

    srand(time(NULL));
    
    /** Parse Arguments */
    if(argc >= 3) {
        k = atoi(argv[1]);
        m = atoi(argv[2]);
    }
    else
    {
        printf("Please enter the following parameters :\n");
        printf("K : ");
        scanf("%d", &k);
        printf("M : ");
        scanf("%d", &m);
    }


    /** Allocate shared memory */
    createSharedMatrix(&matrix, k, m);
    products = sharedMalloc(m*sizeof(float));
    
    /** Save start up time */
    clock_gettime(CLOCK_REALTIME, &startUpTime);
    
    
    
    /** Computation **/

    findMinMax(matrix, k, m);
    
    for(i=0 ; i<m ; i++) {
        products[i] = innerProduct(matrix, k, m , i);
    }
    
    showVarMinMax(products, m);
    

    /** Save end time */
    clock_gettime(CLOCK_REALTIME, &endTime);
    
    /** Find runtime */
    float sec = (float) (endTime.tv_sec - startUpTime.tv_sec);
    float nsec = ((float) (endTime.tv_nsec - startUpTime.tv_nsec)) / 1000000000.0;
    
    /** Display informations about the run */
    printf("\nProg1 --- K = %d ; M = %d ; Runtime : %f seconds\n\n",
             k, m, sec + nsec);
    

    /** Free **/
    
    shmdt(matrix);
    shmdt(products);
    
    freeSharedMemory();
    
    return 0;
}
