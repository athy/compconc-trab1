program_NAME := exec

common_SRC := MatrixTools.c

program1 := prog1
program1_SRC := Prog1.c $(common_SRC)
program1_OBJS := ${program1_SRC:.c=.o}

program2 := prog2
program2_SRC := Prog2.c $(common_SRC)
program2_OBJS := ${program2_SRC:.c=.o}

program3 := prog3
program3_SRC := Prog3.c $(common_SRC)
program3_OBJS := ${program3_SRC:.c=.o}

#CPPFLAGS += -lrt 
ENDFLAGS = -lrt

.PHONY: all clean distclean

all: $(program1) $(program2) $(program3)

$(program1): $(program1_OBJS)
	$(LINK.cc) $(program1_OBJS) -o $(program1) $(ENDFLAGS)

$(program2): $(program2_OBJS)
	$(LINK.cc) $(program2_OBJS) -o $(program2) $(ENDFLAGS)
	
$(program3): $(program3_OBJS)
	$(LINK.cc) $(program3_OBJS) -o $(program3) $(ENDFLAGS)

clean:
	@- $(RM) $(program1) $(program2) $(program3)
	@- $(RM) $(program1_OBJS) $(program2_OBJS) $(program3_OBJS)
	@- $(RM) *~

distclean: clean
