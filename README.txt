########################################
#########   README.txt     #############
########################################



#### Build:
To build the 3 versions of the program just run make :
$ make



#### Run :

You can pass k and m as arguments of the program :

$ ./prog1 16 100

will run the first version with k=16 and m=100

If they are not provided k and m will be ask will running



For the third version of the program (prog3) you can pass the number of subprocess as the third argument :
$ ./prog3 k m nbSubProc

It can also be done this way (in this case, k and m will be asked while running) :
$ ./prog3 nbSubProc

The default number of subprocesses is 2