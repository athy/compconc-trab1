
#include "MatrixTools.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/shm.h>
#include <math.h>


static int *sharedMemIds = 0;
static int sharedMemIdsIndex = 0;
static int sharedMemIdsSize = 0;


void findMinMax(float *matrix, const int k, const int m)
{
    int i,j, maxI, maxJ, minI, minJ;
    float min = 51;
    float max = -51;

    for(i=0 ; i< k ; i++) {
        for(j=0 ; j<m ; j++) {
            if(matrix[i + j*k] < min) {
                min = matrix[i + j*k];
                minI = i;
                minJ = j;
            }
            if(matrix[i + j*k] > max) {
                max = matrix[i + j*k];
                maxI = i;
                maxJ = j;
            }
        }
    }

    if(abs(max) > 50 || abs(min > 50)) {
        fprintf(stderr, "Error : Matrix is not bounded between -50 and +50\n");
        exit(1);
    }

    printf("\nMatrix : \n");
    printf("Min : %f  Coordinates / (%d, %d)\n", min, minI, minJ);
    printf("Max : %f  Coordinates / (%d, %d)\n\n", max, maxI, maxJ);

}

void printMatrix(float *matrix, const int k, const int m)
{
    int i, j;

    for(i=0 ; i<k ; i++)
    {
        for(j=0 ; j<m ; j++) {
            if((matrix[i + j*k]) >= 0)
                printf(" ");
            if(abs(matrix[i+j*k]) < 10)
                printf(" ");
            printf(" %f", matrix[i+j*k]);
        }
        printf("\n");
    }
}

inline float random_float()
{
     float res = (float) rand();
     res = (res / (float) RAND_MAX * 100) - 50.;
     return res;
}

void createSharedMatrix(float **matrix, const int k, const int m)
{
    int i=0;
    int j=0;
    int shmid;

    (*matrix) = sharedMalloc(k * m * sizeof(float));

    for(i=0 ; i<k ; i++) {
        for(j=0 ; j<m ; j++)
            (*matrix)[i + j*k] = random_float();
    }
    
}

float innerProduct(float *matrix, const int k, const int m, const int i)
{
    float result = 0;
    int j;
    
    for(j=0 ; j<k ; j++) {
        result += matrix[j + i*k] * matrix[j + i*k];
    }
    
    return result;
}

void showVarMinMax(float *array, const int size) 
{
    int i;
    float min =  99999999999999999.0;
    float max = -99999.0;
    float average = 0.0;
    float var = 0.0;
    
    for(i=0 ; i<size ; i++) {
        average += array[i];
        
        if(array[i] < min) 
            min = array[i];
        
        if(array[i] > max) 
            max = array[i];
    }
    average = average / ((float) size);
    
    for(i=0 ; i<size ; i++) 
        var += (average - array[i])*(average - array[i]);
    
    var = sqrt(var);
    
    printf("Inner products :\n");
    printf("Min : %.3f ; Max : %.3f ; Variance : %.3f\n", min, max, var);
}

void *sharedMalloc(int size)
{
    int shmid;
    void *pt;
    
    if(sharedMemIds == NULL) {
        sharedMemIds = malloc(100 * sizeof(int));
        sharedMemIdsSize = 100;
    }
    if(sharedMemIdsIndex >= sharedMemIdsSize) {
        int * tmp = sharedMemIds;
        sharedMemIds = malloc(sharedMemIdsSize * 2 * sizeof(int));
        memcpy(sharedMemIds, tmp, sharedMemIdsSize * sizeof(int));
        sharedMemIdsSize *= 2;
        free(tmp);
    }
    
    if((shmid = shmget(IPC_PRIVATE, size, IPC_CREAT | 0666)) < 0) {
        perror("Matrix Allocation Error : shmget");
        fprintf(stderr, "The matrix you requested was probably to big to be in shared memory\n");
        freeSharedMemory();
        exit(1);
    }
    
    if((pt = shmat(shmid, NULL, 0)) == (void*) -1) {
        perror("Matrix Allocation Error : shmat");
        freeSharedMemory();
        exit(1);
    }
    sharedMemIds[sharedMemIdsIndex++] = shmid;
    
    return pt;
    
}

void freeSharedMemory()
{
    int i;
    for(i=0 ; i<sharedMemIdsIndex ; i++) {
        if (shmctl(sharedMemIds[i], IPC_RMID, 0) == -1) {
            perror("shmctl RMID");
            exit(1);
        }
    }
    
    free(sharedMemIds);
    sharedMemIds = NULL;
}