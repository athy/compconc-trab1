/*
 ============================================================================
 Name        : Prog3.c
 Author      : Arthur Bit-Monnot
               Marcelo Araújo Carvalho
               Jonatas Handam Santos
 Description : Matrix manipulation with a number of subprocesses passed as 
               an argument at runtime.
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

#include "MatrixTools.h"



int main(int argc, char *argv[])
{
    /** Declarations */
    int m, k, i, localNext, fatherPid;
    float *matrix;
    float *products;
    int *nextProduct;
    int nbProc = 2;
    struct timespec startUpTime, endTime;

    srand(time(NULL));
    
    /** Parse Arguments */
    if(argc >= 3) {
        k = atoi(argv[1]);
        m = atoi(argv[2]);
        
        if(argc >= 4)
            nbProc = atoi(argv[3]);
    }
    else
    {
        printf("Please enter the following parameters :\n");
        printf("K : ");
        scanf("%d", &k);
        printf("M : ");
        scanf("%d", &m);
        if(argc >= 2)
            nbProc = atoi(argv[1]);
    }


    /** Allocate shared memory */
    createSharedMatrix(&matrix, k, m);
    products = sharedMalloc(m*sizeof(float));
    nextProduct = sharedMalloc(sizeof(int));
    *nextProduct = 0;
    
    /** Store the father's pid */
    fatherPid = getpid();
    
    /** Save start up time */
    clock_gettime(CLOCK_REALTIME, &startUpTime);
    
    
    
    /****** Computation *******/

    findMinMax(matrix, k, m);
    
    /** Creates nbProc subprocess and let them run same code as the father */
    for(i=0 ; i<nbProc ; i++) {
        if(fork() == 0) {
            break;
        }
    }
    /** The next two lines are a critical region. Interruption between those two lines might 
     * result in a product being calculated twice. Very unlikely and won't end in corrupted data.
     * Probably not worthy to had access control */
    localNext = *nextProduct;
    (*nextProduct) = localNext+1;
    
    while(localNext < m) {
        products[localNext] = innerProduct(matrix, k, m , localNext);
        
        /** The next two lines are a critical region. Interruption between those two lines might 
         * result in a product being calculated twice. Very unlikely and won't end in corrupted data.
         * Probably not worthy to had access control */
        localNext = *nextProduct;
        (*nextProduct) = localNext+1;
    }
    
    if(fatherPid == getpid()) {
        /** I'm the father, I wait for my children */
        for(i=0 ; i<nbProc ; i++) 
            wait();
    } else {
        /** I'm a subprocess and there's nothing more to compute */
        exit(0);
    }
    
    showVarMinMax(products, m);
    
    
    /** Save end time */
    clock_gettime(CLOCK_REALTIME, &endTime);
    
    /** Find runtime */
    float sec = (float) (endTime.tv_sec - startUpTime.tv_sec);
    float nsec = ((float) (endTime.tv_nsec - startUpTime.tv_nsec)) / 1000000000.0;
    
    /** Display informations about the run */
    printf("\nProg3 --- K = %d ; M = %d ; SubProcesses = %d ; runtime : %f seconds\n\n",
             k, m, nbProc, sec + nsec);
    
    
    
    /******* Free memory  *********/
    
    shmdt(matrix);
    shmdt(products);
    shmdt(nextProduct);
    
    freeSharedMemory();
    
    return 0;
}
