#ifndef MATRIXTOOLS_H
#define MATRIXTOOLS_H


/**
 * Dynamically allocate a m * k matrix.
 * Randomly initiate it with floats between -50 and 50
 */
void createSharedMatrix(float **matrix, const int k, const int m);

/**
 * Print the matrix to standard output
 */
void printMatrix(float *matrix, const int k, const int m);

/**
 * Find the higher and lower term of the matrix and displays
 * them with their respective coordinates.
 */
void findMinMax(float *matrix, const int k, const int m);

/**
 * Compute the inner product of the matrix
 */
float innerProduct(float *matrix, const int k, const int m, const int i);

/**
 * Display the maximum value, the minimum value and the variance 
 * of the passed as argument
 */
void showVarMinMax(float *array, const int size);

/**
 * Dinamically allocate shared memory
 * @return pointer to the allocated memory
 */
void * sharedMalloc(int size);

/**
 * Free every shared memory that has been allocated using sharedMalloc()
 */
void freeSharedMemory();

#endif